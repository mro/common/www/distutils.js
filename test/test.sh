#!/bin/bash

function die() {
  echo "$1" >&2
  exit 1
}

TESTDIR=$(cd $(dirname $0); pwd)

"${TESTDIR}/prepare/test.sh" || die 'Prepare test failed'
"${TESTDIR}/fetch/test.sh" || die 'Fetch test failed'

