#!/bin/bash

function die() {
  echo "$1" >&2
  exit 1
}

cd $(dirname $0)

rm -rf out
../../src/prepare.js out || die "Prepare failed"

for f in "out/test-1.js" "out/test-1.2.js" "out/test-1.2.3.js" "out/test-prepare.js"; do
  [ -e "$f" ] || die "No such file $f"
done

echo "Ok"
