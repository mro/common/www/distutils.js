#!/bin/bash

function die() {
  echo "$1" >&2
  exit 1
}

cd $(dirname $0)

rm -rf dist
../../src/fetch.js || die "Fetch failed"

for f in "dist/test.repo" "dist/test-fetch.repo"; do
  [ -e "$f" ] || die "No such file $f"
done

echo "Ok"
