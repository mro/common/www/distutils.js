// @ts-check

const
  process = require("process"),
  path = require("path"),
  getos = require("getos"),
  { promisify } = require("util"),
  { kebabCase, get, mapValues, assign } = require("lodash");

/** @type {distutils.Config} */
const config = (() => {
  try {
    /* eslint-disable-next-line global-require */
    return require(path.resolve(".distutils.json"));
  }
  catch (e) {
    return {};
  }
})();
config.url = config.url || "http://mro-dev.web.cern.ch/distfiles/js/";
config.dist = config.dist || "dist";

/**
 * @param  {distutils.PackageJson} pkg
 */
async function makeEnv(pkg) {
  const osInfo = mapValues(await promisify(getos)(), (v) => kebabCase(v));
  const name = get(pkg, [ "name" ], "");
  const version = get(pkg, [ "version" ], "");

  return assign({
    name: name.replace(/^@\w+\//, ""),
    version: version,
    major: version.split(".")[0],
    minor: version.split(".")[1],
    patch: version.split(".")[2],

    arch: process.arch,
    platform: process.platform
  }, osInfo);
}

/**
 * @param {string} string
 * @param {any} repl
 * @param {boolean} [keep]
 * @return {string}
 */
function format(string, repl, keep) {
  return string.replace(/{([^\}]*)}/g, function(match, key) {
    const r = repl[key];
    if (r === undefined) {
      return keep ? match : "";
    }
    else {
      return r;
    }
  });
}

/**
 * @param  {string} file
 * @param {string} [dist]
 * @return {string}
 */
function resolvePath(file, dist) {
  dist = dist || config.dist;
  if (!file) {
    return file;
  }
  else if (file[0] === "/") {
    return file.substring(1);
  }
  else if (file.startsWith("./")) {
    return file.substring(2);
  }
  else {
    return path.join(dist, file);
  }
}

/**
 * @param  {string} url
 */
function resolveUrl(url) {
  return (/:\/\//.test(url)) ? url : (config.url + url);
}

module.exports = { config, resolvePath, resolveUrl, makeEnv, format };
