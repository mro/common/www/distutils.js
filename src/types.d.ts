// @ts-check

export = distutils;
export as namespace distutils;

declare namespace distutils {
  interface Config {
    url: string,
    dist: string
  }

  interface PackageJson {
    name: string,
    version: string,
    distfiles: { [name: string]: string }
  }
}


