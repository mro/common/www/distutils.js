#!/usr/bin/env node
// @ts-check

const
  ProgressBar = require("progress"),
  path = require("path"),
  fs = require("fs"),
  utils = require("./utils"),
  { mkdirp } = require("mkdirp"),
  URL = require("url");

/** @type {distutils.PackageJson} */
const pkg = require(path.resolve("package.json"));

/**
 * @param {string} file
 * @param {string} dest
 */
async function copy(file, dest) {
  const writer = fs.createWriteStream(dest);

  const reader = fs.createReadStream(file);

  const total = fs.statSync(file).size;
  const progress = new ProgressBar(
    "Copying [:bar] :percent (ETA::etas) ",
    {
      width: 40, complete: "=", incomplete: "-", renderThrottle: 250,
      head: ">", clear: true,
      total: total, stream: process.stdout
    });

  reader.on("data",
    (/** @type {{ length: number }} */ chunk) => progress.tick(chunk.length));

  reader.pipe(writer);

  return new Promise((resolve, reject) => {
    writer.on("finish", resolve);
    writer.on("error", reject);
  });
}

/**
 * @param  {distutils.PackageJson} pkg
 * @param  {any} env
 * @param  {string} distdir
  */
async function prepare(pkg, env, distdir) {
  if (pkg.distfiles) {
    await mkdirp(distdir);

    for (const file in pkg.distfiles) {
      if (!pkg.distfiles.hasOwnProperty(file)) {
        continue;
      }

      const url = utils.resolveUrl(utils.format(file, env, true));
      let dest = path.basename(URL.parse(url).pathname || "");
      if (!dest) {
        throw new Error("Destination file not found");
      }

      dest = utils.resolvePath(dest, distdir);
      const src = utils.resolvePath(
        utils.format(pkg.distfiles[file], env, true));

      if (src === dest) {
        console.log("No preparation required for:", src);
      }
      else {
        await copy(src, dest)
        .then(
          () => { console.log("File copied:", src, "->", dest); },
          (e /*: any */) => {
            console.log(`Failed to copy (${src} -> ${dest}):`, e.message);
            throw e;
          });
      }
    }
  }
}

const distdir = (process.argv.length > 2) ? process.argv[2] : utils.config.dist;

(async () => {
  try {
    const env = await utils.makeEnv(pkg);
    await prepare(pkg, env, distdir);
  }
  catch (err) {
    if (process.env.DEBUG) {
      console.log(err);
    }
    process.exitCode = 1;
  }
})();
