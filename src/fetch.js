#!/usr/bin/env node
// @ts-check

const
  axios = require("axios").default,
  path = require("path"),
  fs = require("fs"),
  { mkdirp } = require("mkdirp"),
  ProgressBar = require("progress"),
  utils = require("./utils");

/** @type {distutils.PackageJson} */
const pkg = require(path.resolve("package.json"));

/**
 * @typedef {import('axios').AxiosResponse<any>} AxiosResponse
 * @typedef {import('axios').AxiosRequestConfig} AxiosRequestConfig
 */

const MAX_RETRY = 5;

/**
 * @param {AxiosResponse & { config: AxiosRequestConfig & { _retry?: number } }} err
 */
function retryFailedRequest(err) {
  if (err.status !== 404 && (err.config?._retry ?? 0) < MAX_RETRY) {
    err.config._retry = (err.config?._retry ?? -1) + 1;
    console.warn(`Request failed: ${err.statusText}, retrying ...`);
    return axios(err.config);
  }
  throw err;
}
axios.interceptors.response.use(undefined, retryFailedRequest);

/**
 * @param {string} url
 * @param {string} path
 */
async function download(url, path) {
  console.log("Starting to download:", url);
  const response =
    await axios({ url, method: "GET", responseType: "stream" });

  if (response.headers && response.headers["content-length"]) {
    const total = response.headers["content-length"];
    const progress = new ProgressBar(
      "Downloading [:bar] :percent (ETA::etas) ",
      {
        width: 40, complete: "=", incomplete: "-", renderThrottle: 250,
        head: ">", clear: true,
        total: parseInt(total, 10), stream: process.stdout
      });

    response.data.on("data",
      (/** @type {{ length: number }} */ chunk) => progress.tick(chunk.length));
  }

  const writer = fs.createWriteStream(path);
  response.data.pipe(writer);

  return new Promise((resolve, reject) => {
    writer.on("finish", resolve);
    writer.on("error", reject);
  });
}

/**
 * @param {distutils.PackageJson} pkg
 * @param {any} env
 */
async function prefetch(pkg, env) {
  if (process.env.NO_DIST_FETCH &&
      (process.env.NO_DIST_FETCH.split(",").indexOf(pkg.name) >= 0)) {
    console.log("Fetch disabled for package:", pkg.name);
    throw new Error("Fetch disabled from environment: NO_DIST_FETCH=" +
      process.env.NO_DIST_FETCH);
  }
  else if (pkg.distfiles) {
    for (const file in pkg.distfiles) {
      if (!pkg.distfiles.hasOwnProperty(file)) {
        continue;
      }

      const url = utils.resolveUrl(utils.format(file, env, true));
      const dest = utils.resolvePath(
        utils.format(pkg.distfiles[file], env, true));

      await mkdirp(path.dirname(dest));
      await download(url, dest)
      .then(
        () => { console.log("File downloaded:", dest); },
        (e /*: any */) => {
          console.log(`Failed to fetch (${url}):`, e.message);
          throw e;
        });
    }
  }
}

(async () => {
  try {
    const env = await utils.makeEnv(pkg);
    await prefetch(pkg, env);
  }
  catch (err) {
    if (process.env.DEBUG) {
      console.log(err);
    }
    process.exitCode = 1;
  }
})();
