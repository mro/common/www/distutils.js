# Dist utilities for Node.js

A set of command-line utilities to manage prebuilt binaries or packed artifacts.

## dist-fetch utility

dist-fetch is used to download cached build artifacts from a build server.

### Usage

From command-line:
```bash
dist-fetch || npm run build
```

From `package.json`:
```js
{
  ...,
  "scripts": {
    "postinstall": "pwd | grep -v -q /node_modules/ || dist-fetch"
  },
  "distfiles": {
    "my-package-{version}.js": "index.js",
    "http://my-url/my-package-{major}-{minor}.js": "/dist/index.js",
    "my-package-{major}-{minor}.js": "./dist/index-{major}.js",
  }
}
```

Example output:
```bash
Starting to download: https://mro-dev.web.cern.ch/distfiles/cc7/cquery-1.0.0-Linux.rpm
Downloading [===>------------------------------------] 9% (ETA:27.3s)
```

## dist-prepare utility

dist-fetch is used to prepare build artifacts before uploading it to the build server.

### Usage

From command-line:
```bash
# creates files in configured "dist" (default: dist)
dist-prepare

# creating files in my-dir
dist-prepare my-dir
```

## Configuration

A `.distutils.json` file can be created in the project's root directory,
with the following syntax:
```js
{
  // Base url added to any source that doesn't start with 'proto://'
  "url": "https://base-url",
  // relative directory path where to download files,
  // added to any destination that doesn't start with "./" or "/"
  "dist": "dist"
}
```

The following variables can be used both in source and destinationn parts:

| variable | description                      | content              |
| -------- | -------------------------------- | -------------------- |
| name     | project name                     | package.json#name    |
| version  | project version                  | package.json#version |
| major    | major version num                | package.json#version |
| minor    | minor version num                | package.json#version |
| patch    | patch version num                | package.json#version |
| arch     | current arch                     | [process.arch](https://nodejs.org/api/process.html#process_process_arch) |
| platform | current platform                 | [process.platform](https://nodejs.org/api/process.html#process_process_platform) |
| os       | similar to platform              | see [getos](https://github.com/retrohacker/getos) |
| dist     | distribution name                | see [getos](https://github.com/retrohacker/getos) |
| codename | distribution codename (optional) | see [getos](https://github.com/retrohacker/getos) |
| dist     | distribution name (optional)     | see [getos](https://github.com/retrohacker/getos) |
| release  | distribution release (optional)  | see [getos](https://github.com/retrohacker/getos) |
